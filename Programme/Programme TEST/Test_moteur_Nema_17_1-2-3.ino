# include <Stepper.h>

Stepper myStepper1(3200, 2, 5);
Stepper myStepper2(3200, 3, 6);
Stepper myStepper3(3200, 4, 7);

void setup() {
  myStepper1.setSpeed(100);
  myStepper2.setSpeed(100);
  myStepper3.setSpeed(100);
}

void loop() {
  myStepper3.step(10000);
  delay(500);
  myStepper2.step(10000);
  delay(500);
  myStepper3.step(10000);
  delay(500);
  myStepper3.step(-10000);
  delay(500);
  myStepper2.step(-10000);
  delay(500);
  myStepper3.step(-10000);
  delay(500);
}
