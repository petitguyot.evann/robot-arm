# include <Stepper.h>

#define Enregistrement 12 // definire la broche 12 sur le mot enregistrement
#define Lancement 13      // definire la broche 13 sur le mot lancement
int enregistrement = 0;
int lancement = 0;

int positionX = 0; // compteur des déplacements apres chaque enregistrements de position pour X
int positionY = 0; //                                                                         Y
int positionZ = 0; //                                                                         Z

int chemin[5];  // tableau pour enregistrer les coordonnées du robot
int postab = 0; // pointeur du tableau (postab = position tableau)
int pos = 0;    // scegond pointeur du tableau (pos = position)

int homeX = 0; // compteur pour retourner a la position ilitial X
int homeY = 0; //                                               Y
int homeZ = 0; //                                               Z

#define joyX A0 // axe X du joystique defini sur la broche A0
int xValue = 0; // cette variable servira a faire varier la vitesse d'un moteur X

#define joyY A1 // axe Y du joystique defini sur la broche A1
int yValue = 0; // cette variable servira a faire varier la vitesse d'un moteur Y

#define joyZ A2 // axe X du joystique 2 defini sur la broche A2
int zValue = 0; // cette variable servira a faire varier la vitesse d'un moteur Z

const int nbPas = 3200; // nombre de pas pour 1 tour

Stepper myStepperX(nbPas, 2, 5); // definition des pin pour les driveur moteur    X
Stepper myStepperY(nbPas, 3, 6); // DIR / STEP                                    Y
Stepper myStepperZ(nbPas, 4, 7); //                                               Z



// Setup --------------------------------------------------------------------------------------------------------------------------

void setup() {
  Serial.begin (9600); // initialisation du port serie
  pinMode(Enregistrement, INPUT_PULLUP); // dire que enregistrement est une entrée muni d'une résistance (pullup)
  pinMode(Lancement, INPUT_PULLUP);
}



// Loop ---------------------------------------------------------------------------------------------------------------------------

void loop() {
  enregistrement = digitalRead(Enregistrement);
  lancement = digitalRead(Lancement);

  xValue = analogRead(joyX); // lire joyX (A0) et ecrire cette valeur sur xValue
  yValue = analogRead(joyY);
  zValue = analogRead(joyZ);

  // Déplacements -----------------------------------------------------------------------------------------------------------------

  // X ------------
  if (xValue > 520) { // x / +
    xValue = map(xValue, 520, 1010, 30, 1000); // faire varier la vitesse de 30 à 1000 en fonction de la valeur recu
    myStepperX.setSpeed(xValue); // modifier la vitesse du moteur
    myStepperX.step(10); // déplacer le moteur de 10 pas
    positionX = positionX + 1; // ajouter +1 a la position
    homeX = homeX + 1;
  }
  if (xValue < 500) { // x / -
    xValue = map(xValue, 500, 0, 30, 1000); // faire varier la vitesse de 30 à 1000 en fonction de la valeur recu
    myStepperX.setSpeed(xValue); // modifier la vitesse du moteur
    myStepperX.step(-10); // déplacer le moteur de -10 pas
    positionX = positionX - 1; // ajouter -1 a la position
    homeX = homeX - 1;
  }


  // Y ------------
  if (yValue > 520) { // y / +
    yValue = map(yValue, 520, 1010, 30, 1000);
    myStepperY.setSpeed(yValue);
    myStepperY.step(10);
    positionY = positionY + 1;
    homeY = homeY + 1;
  }
  if (yValue < 500) { // y / -
    yValue = map(yValue, 500, 0, 30, 1000);
    myStepperY.setSpeed(yValue);
    myStepperY.step(-10);
    positionY = positionY - 1;
    homeY = homeY - 1;
  }


  // Z ------------
  if (zValue > 520) { // z / +
    zValue = map(zValue, 520, 1010, 30, 1000);
    myStepperZ.setSpeed(zValue);
    myStepperZ.step(10);
    positionZ = positionZ + 1;
    homeZ = homeZ + 1;
  }
  if (zValue < 500) { // z / -
    zValue = map(zValue, 500, 0, 30, 1000);
    myStepperZ.setSpeed(zValue);
    myStepperZ.step(-10);
    positionZ = positionZ - 1;
    homeZ = homeZ - 1;
  }


  // Enregistrements des positions ------------------------------------------------------------------------------------------------

  if (enregistrement == LOW) {
    chemin[postab] = positionX; // enregistrer la position X
    postab = postab + 1; // ce décaler de 1 dans le tableau
    chemin[postab] = positionY; //                         Y
    postab = postab + 1;
    chemin[postab] = positionZ; //                         Z
    postab = postab + 1;
    positionX = 0; // renitialiser la position X
    positionY = 0; //                          Y
    positionZ = 0; //                          Z
    delay (500);
  }


  // Lancement de la scéquence ----------------------------------------------------------------------------------------------------

  if (lancement == LOW) {

    // Retoure au point initial ------------

    myStepperX.setSpeed(500); // motifier la vitesse de laxe X
    myStepperY.setSpeed(500); //                             Y
    myStepperZ.setSpeed(500); //                             Z

    homeX = homeX * 10; // calculer combier de pas il faut pour retourer au home pour X
    homeY = homeY * 10; //                                                            Y
    homeZ = homeZ * 10; //                                                            Z

    myStepperX.step(homeX);
    myStepperX.step(homeY);
    myStepperX.step(homeZ);

    //homeX = 0;
    //homeY = 0;
    //homeZ = 0;

    delay(500);

    // démarage du chemin ------------

    for (int i = 0; i < postab; i++) {
      positionX = chemin[pos] * 10; // calculer le nombre de pas pour X
      pos = pos + 1; // déplacer le pointeur
      positionY = chemin[pos] * 10;//                                 Y
      pos = pos + 1;
      positionZ = chemin[pos] * 10;//                                 Z
      pos = pos + 1;

      myStepperX.step(positionX);
      myStepperX.step(positionY);
      myStepperX.step(positionZ);

      delay(500);
    }
    pos = 0; // rénitialiser le pointeur
  }

  // Débug ------------

  Serial.print("X:");
  Serial.print(positionX);
  Serial.print("  Y:");
  Serial.print(positionY);
  Serial.print("  Z:");
  Serial.print(positionZ);

  Serial.print("  /  ");

  Serial.print("home-X:");
  Serial.print(homeX);
  Serial.print("  home-Y:");
  Serial.print(homeY);
  Serial.print("  home-Z:");
  Serial.print(homeZ);

  Serial.print("  ");
  Serial.print(enregistrement);
  Serial.print(" / ");
  Serial.println(lancement);
}
