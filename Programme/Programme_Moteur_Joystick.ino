# include <Stepper.h>

#define joyX A0 // axe X du joystique defini sur la broche A0
int xValue = 0; // cette variable servira a faire varier la vitesse d'un moteur X

#define joyY A1 // axe Y du joystique defini sur la broche A1
int yValue = 0; // cette variable servira a faire varier la vitesse d'un moteur Y

#define joyZ A2 // axe X du joystique 2 defini sur la broche A2
int zValue = 0; // cette variable servira a faire varier la vitesse d'un moteur Z

const int nbPas = 3200; // nombre de pas pour 1 tour

Stepper myStepperX(nbPas, 2, 5); // definition des pin pour les driveur moteur    X
Stepper myStepperY(nbPas, 3, 6); // DIR / STEP                                    Y
Stepper myStepperZ(nbPas, 4, 7); //                                               Z



// Setup --------------------------------------------------------------------------------------------------------------------------

void setup() {
}



// Loop ---------------------------------------------------------------------------------------------------------------------------

void loop() {
  xValue = analogRead(joyX); // lire joyX (A0) et ecrire cette valeur sur xValue
  yValue = analogRead(joyY);
  zValue = analogRead(joyZ);

  // Déplacements -----------------------------------------------------------------------------------------------------------------

  // X ------------
  if (xValue > 520) { // x / +
    xValue = map(xValue, 520, 1010, 30, 1000); // faire varier la vitesse de 30 à 1000 en fonction de la valeur recu
    myStepperX.setSpeed(xValue); // modifier la vitesse du moteur
    myStepperX.step(10); // déplacer le moteur de 10 pas
  }
  if (xValue < 500) { // x / -
    xValue = map(xValue, 500, 0, 30, 1000); // faire varier la vitesse de 30 à 1000 en fonction de la valeur recu
    myStepperX.setSpeed(xValue); // modifier la vitesse du moteur
    myStepperX.step(-10); // déplacer le moteur de -10 pas
  }


  // Y ------------
  if (yValue > 520) { // y / +
    yValue = map(yValue, 520, 1010, 30, 1000);
    myStepperY.setSpeed(yValue);
    myStepperY.step(10);
  }
  if (yValue < 500) { // y / -
    yValue = map(yValue, 500, 0, 30, 1000);
    myStepperY.setSpeed(yValue);
    myStepperY.step(-10);
  }


  // Z ------------
  if (zValue > 520) { // z / +
    zValue = map(zValue, 520, 1010, 30, 1000);
    myStepperZ.setSpeed(zValue);
    myStepperZ.step(10);
  }
  if (zValue < 500) { // z / -
    zValue = map(zValue, 500, 0, 30, 1000);
    myStepperZ.setSpeed(zValue);
    myStepperZ.step(-10);
  }
}
