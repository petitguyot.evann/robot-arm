# Robot ARM
![Alt text](F8C3C5B3-2331-4772-B46B-7F3100E24020_1_105_c.jpeg)
![Alt text](55BD1C73-1C21-490D-B0F5-DF390709FA4B_1_105_c.jpeg)
![Alt text](E4F1462A-1B4A-4698-AB97-AC1EC88FA5FA_1_105_c.jpeg)

# Sources

-https://www.thingiverse.com/thing:1774473
-https://github.com/uStepper/uStepper-RobotArm-Rev3



# Matos électronique

-Arduino UNO
-CNC shield
-x3 Stepper motor driver (A4988)
-x3 Moteur Nema 17
-Servo moteur
-x2 Joystick
-Alimentation 12V
-Interrupteur ON / OFF